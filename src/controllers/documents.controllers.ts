import { Request, Response } from "express";
import Documents from "../class/documents";
import {
  IErrorDataBase,
  ISuccessOperation,
} from "../interfaces/database.interface";
import { IDocuments } from "../interfaces/documents.interface";

const listDocuments = async (req: Request, res: Response) => {
  const document = new Documents();
  const result: IDocuments | IErrorDataBase = await document.listDocuments();
  res.json(result);
};

const listDocument = async (req: Request, res: Response) => {
  const parameter: IDocuments = {
    id: parseInt(req.params.id)
  }
  const document = new Documents(parameter);
  const result: IDocuments | IErrorDataBase = await document.listDocument();
  res.json(result);
};

const insertDocument = async (req: Request, res: Response) => {
  const parameter: IDocuments = req.body;
  const document = new Documents(parameter);
  const result: ISuccessOperation | IErrorDataBase = await document.insertDocument();
  if ((result as ISuccessOperation).affectedRows !== undefined) {
    const message = (result as ISuccessOperation).affectedRows > 0
        ? "Insercion completada 😀"
        : `No se realizo la operacion 😥`;
    res.json({
      msg: message,
    });
  } else {
    res.json({
      msg: (result as IErrorDataBase).message,
    });
  }
};

const updateDocument = async (req: Request, res: Response) => {
  const parameter: IDocuments = {
    id: parseInt(req.params.id),
    direction: req.body.direction,
  };
  const document = new Documents(parameter);
  const result: ISuccessOperation | IErrorDataBase = await document.updateDocument();
  if ((result as ISuccessOperation).affectedRows !== undefined) {
    const message = (result as ISuccessOperation).affectedRows > 0
        ? "Actualizacion completada 😀"
        : `No se realizo la operacion 😥`;
    res.status(200).json({
      msg: message
    });
  } else {
    res.status(404).json({
      msg: (result as IErrorDataBase).message
    });
  }
};

export { listDocuments, listDocument, insertDocument, updateDocument };
