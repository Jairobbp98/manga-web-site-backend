import { IErrorDataBase, ISuccessOperation } from "./database.interface";

interface IChapters {
    id?: number;
    nom_cap?: string;
    id_img_port?: number;
    id_fecha_estreno?: number;
}

interface IChaptersClass extends IChapters {
    listChapters: () => Promise<IChapters | IErrorDataBase>;
    listChapter: () => Promise<IChapters | IErrorDataBase>;
    deleteChapter: () => Promise<ISuccessOperation | IErrorDataBase>;
    insertChapter: () => Promise<ISuccessOperation | IErrorDataBase>;
    updateChapter: () => Promise<ISuccessOperation | IErrorDataBase>;
}

export { IChapters, IChaptersClass }