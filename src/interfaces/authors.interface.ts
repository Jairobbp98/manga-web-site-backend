import { IErrorDataBase, ISuccessOperation } from "./database.interface";

interface IAuthors {
    id?: number;
    nameAuthor?: string;
}

interface IAuthorsClass extends IAuthors {
    listAuthor: () => Promise<IAuthors | IErrorDataBase>;
    listAuthors: () => Promise<IAuthors | IErrorDataBase>;
    insertAuthor: () => Promise<ISuccessOperation | IErrorDataBase>;
    updateAuthor: () => Promise<ISuccessOperation | IErrorDataBase>;
    deleteAuthor: () => Promise<ISuccessOperation | IErrorDataBase>;
} 

export { IAuthors, IAuthorsClass }