import { IErrorDataBase, ISuccessOperation } from "./database.interface";

interface IGenders {
    id?: number;
    gender?: string;
}

interface IGendersClass extends IGenders {
    listGender: () => Promise<IGenders | IErrorDataBase>;
    listGenders: () => Promise<IGenders | IErrorDataBase>;
    insertGender: () => Promise<ISuccessOperation | IErrorDataBase>;
    updateGender: () => Promise<ISuccessOperation | IErrorDataBase>;
    deleteGender: () => Promise<ISuccessOperation | IErrorDataBase>;            
}

export { IGenders, IGendersClass }