import { IErrorDataBase, ISuccessOperation } from "./database.interface";

interface IInformation {
    id?: number;
    idTitulo?: number;
    idImgPort?: number;
    idDoc?: number;
    idAuthors?: number;
    idGeneros?: number;
    idFechaEstreno?: number;
    idEstado?: number;
    idDescripcion?: number;
}

interface IInformationClass extends IInformation {
    listInformations: () => Promise<IInformation | IErrorDataBase>;
    listInformation: () => Promise<IInformation | IErrorDataBase>;
    insertInformation: () => Promise<ISuccessOperation | IErrorDataBase>;
    updateInformation: () => Promise<ISuccessOperation | IErrorDataBase>;
    deleteInformation: () => Promise<ISuccessOperation | IErrorDataBase>;
}

export { IInformation, IInformationClass }