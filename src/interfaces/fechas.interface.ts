import { IErrorDataBase, ISuccessOperation } from "./database.interface";

interface IFecha {
    id?: number;
    fecha?: string;
}

interface IFechaClass extends IFecha {
    listFecha: () => Promise<IFecha | IErrorDataBase>;
    listFechas: () => Promise<IFecha | IErrorDataBase>;
    insertFecha: () => Promise<ISuccessOperation | IErrorDataBase>;
    updateFecha: () => Promise<ISuccessOperation | IErrorDataBase>;
    deleteFecha: () => Promise<ISuccessOperation | IErrorDataBase>;
}

export { IFecha, IFechaClass }