import { IErrorDataBase, ISuccessOperation } from "./database.interface";

interface IEstado{
    id?: number;
    statuss?: string;
}

interface IEstadoClass extends IEstado {
    listEstado: () => Promise<IEstado | IErrorDataBase>;
    listEstados: () => Promise<IEstado | IErrorDataBase>;
    insertEstado: () => Promise<ISuccessOperation | IErrorDataBase>;
    updateEstado: () => Promise<ISuccessOperation | IErrorDataBase>;
    deleteEstado: () => Promise<ISuccessOperation | IErrorDataBase>;
}

export { IEstado, IEstadoClass }