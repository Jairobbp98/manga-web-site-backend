import { Pool } from "mysql2/promise";

interface IDatabase {
  connection: () => Promise<Pool>;
}

interface IConfigDatabase {
  host: string;
  user: string;
  password: string;
  database: string;
  connectionLimit: number;
}

interface ISuccessOperation{
  affectedRows: number;
  fieldCount: number;
  info: string;
  insertId: number;
  serverStatus: number;
  warningStatus: number;
}

interface IErrorDataBase {
  message: string;
  code: string;
  errno: number;
  sqlState: string;
  sqlMessage: string;
}

export { IDatabase, IConfigDatabase, ISuccessOperation, IErrorDataBase };
