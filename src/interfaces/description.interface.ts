import { IErrorDataBase, ISuccessOperation } from "./database.interface";

interface IDescription {
    id?: number;
    descripcion?: string;
}

interface IDescriptionClass extends IDescription{
    listDescription: () => Promise<IDescription | IErrorDataBase>;
    listDescriptions: () => Promise<IDescription | IErrorDataBase>;
    insertDescription: () => Promise<ISuccessOperation | IErrorDataBase>;
    updateDescription: () => Promise<ISuccessOperation | IErrorDataBase>;
    deleteDescription: () => Promise<ISuccessOperation | IErrorDataBase>;
}

export { IDescription, IDescriptionClass }