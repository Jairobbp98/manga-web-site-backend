import { IErrorDataBase, ISuccessOperation } from "./database.interface";

interface ITitles {
    id?: number;
    principalTitle?: string;
    otherTitle?: string;
}

interface ITitlesClass extends ITitles {
    listTitle: () => Promise<ITitles | IErrorDataBase>;
    listTitles: () => Promise<ITitles | IErrorDataBase>;
    insertTitle: () => Promise<ISuccessOperation | IErrorDataBase>;
    updateTitle: () => Promise<ISuccessOperation | IErrorDataBase>;
    deleteTitle: () => Promise<ISuccessOperation | IErrorDataBase>;
}

export { ITitles, ITitlesClass }