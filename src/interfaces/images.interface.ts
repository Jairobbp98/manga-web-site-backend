import { IErrorDataBase, ISuccessOperation } from "./database.interface";

interface IImages {
    id?: number;
    img?: string;
}

interface IImagesClass extends IImages {
    listImage: () => Promise<IImages | IErrorDataBase>;
    listImages: () => Promise<IImages | IErrorDataBase>;
    insertImage: () => Promise<ISuccessOperation | IErrorDataBase>;
    updateImage: () => Promise<ISuccessOperation | IErrorDataBase>;
    deleteImage: () => Promise<ISuccessOperation | IErrorDataBase>; 
}

export { IImages, IImagesClass }