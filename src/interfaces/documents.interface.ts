import { IErrorDataBase, ISuccessOperation } from "./database.interface";

interface IDocuments {
    id?: number;
    direction?: string;
}

interface IDocumentsClass extends IDocuments {
    listDocuments: () => Promise<IDocuments | IErrorDataBase>;
    listDocument: () => Promise<IDocuments | IErrorDataBase>;
    insertDocument: () => Promise<ISuccessOperation | IErrorDataBase>;
    updateDocument: () => Promise<ISuccessOperation | IErrorDataBase>;
    deleteDocument: () => Promise<ISuccessOperation | IErrorDataBase>;
}

export { IDocuments, IDocumentsClass };