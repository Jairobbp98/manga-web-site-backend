import { IErrorDataBase, ISuccessOperation } from "./database.interface";

interface IBook {
    id: number;
    tipo: string;
    id_information: number;
    id_capitulos: number;
    id_clasification: number;
    id_likes: number;
}

interface IBookClass extends IBook {
    listBook: () => Promise<IBook | IErrorDataBase>;
    listBooks: () => Promise<IBook | IErrorDataBase>;
    insertBook: () => Promise<ISuccessOperation | IErrorDataBase>;
    updateBook: () => Promise<ISuccessOperation | IErrorDataBase>;
    deleteBook: () => Promise<ISuccessOperation | IErrorDataBase>;
}

export { IBook, IBookClass }