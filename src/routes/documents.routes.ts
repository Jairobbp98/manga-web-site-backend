import { Router } from 'express'
import { insertDocument, listDocument, listDocuments, updateDocument } from '../controllers/documents.controllers'
const documentsRoutes: Router = Router();

// TODO: listar todos los documentos
documentsRoutes.get('/documents/list', listDocuments);

//TODO: listar un documento en especifico
documentsRoutes.get('/documents/list/:id', listDocument);

//TODO: insertar un documento
documentsRoutes.post('/documents/insert', insertDocument);

//TODO: Actualizar documento
documentsRoutes.put('documents/update/:id', updateDocument)

export default documentsRoutes;