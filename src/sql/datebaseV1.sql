CREATE DATABASE tmangaMysql;
USE tmangaMysql;

CREATE TABLE documents(
    id INT NOT NULL,
    direction VARCHAR(500) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE authors (
    id INT NOT NULL,
    nom_author VARCHAR(100) NOT NULL,
    PRIMARY KEY(ID)
);

CREATE TABLE genders (
    id INT NOT NULL,
    nom_gender VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE images (
    id INT NOT NULL,
    img_url VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE fechas(
    id INT NOT NULL,
    fecha TIMESTAMP DEFAULT current_timestamp,
    PRIMARY KEY(id)
);

CREATE TABLE estado(
    id INT NOT NULL,
    statuss VARCHAR(15) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE information (
    id INT NOT NULL AUTO_INCREMENT,
    principal_title VARCHAR(200) NOT NULL,
    other_title VARCHAR(200) NOT NULL,
    id_img_port INT NOT NULL,
    id_doc INT NOT NULL,
    id_authors INT NOT NULL,
    id_generos INT NOT NULL,
    id_fecha_estreno INT NOT NULL,
    id_status INT NOT NULL,
    description VARCHAR(500) NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (id_img_port) REFERENCES images(id),
    FOREIGN KEY (id_doc) REFERENCES documents(id),
    FOREIGN KEY (id_authors) REFERENCES authors(id),
    FOREIGN KEY (id_generos) REFERENCES genders(id),
    FOREIGN KEY (id_fecha_estreno) REFERENCES fechas(id),
    FOREIGN KEY (id_status) REFERENCES estado(id)
);

CREATE TABLE capitulos (
    id INT NOT NULL,
    nom_cap VARCHAR(100) NULL,
    id_img_port INT NOT NULL,
    id_fecha_estreno INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (id_img_port) REFERENCES images(id),
    FOREIGN KEY (id_fecha_estreno) REFERENCES fechas(id)
);

CREATE TABLE likes (
    id INT NOT NULL,
    num_likes INT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE clasification (
    id INT NOT NULL,
    num_clas DECIMAL NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE books (
    id INT NOT NULL,
    tipo VARCHAR(10) NOT NULL,
    id_information INT NOT NULL,
    id_capitulos INT NOT NULL,
    id_clasification INT NOT NULL,
    id_likes INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (id_information) REFERENCES information(id),
    FOREIGN KEY (id_capitulos) REFERENCES capitulos(id),
    FOREIGN KEY (id_clasification) REFERENCES clasification(id),
    FOREIGN KEY (id_likes) REFERENCES likes(id)
);

CREATE TABLE users (
    id INT NOT NULL,
    user VARCHAR(50) NULL,
    email VARCHAR(100) NOT NULL,
    contraseña VARCHAR(30) NOT NULL,
    id_fecha INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (id_fecha) REFERENCES fechas(id)
);

CREATE TABLE listUserBook (
    id INT NOT NULL,
    id_user INT NOT NULL,
    id_book INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (id_user) REFERENCES users(id),
    FOREIGN KEY (id_book) REFERENCES books(id)
);