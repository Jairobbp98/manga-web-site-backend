import { RowDataPacket, ResultSetHeader } from "mysql2/promise";
import {
  IErrorDataBase,
  ISuccessOperation,
} from "../interfaces/database.interface";
import { IFecha, IFechaClass } from "../interfaces/fechas.interface";
import Database from "./database";

class Fechas implements IFechaClass {
  public id?: number;
  public fecha?: string;
  private database: Database;

  constructor(fecha?: IFecha) {
    this.id = fecha?.id;
    this.fecha = fecha?.fecha;
    this.database = new Database();
  }

  public get getId(): number {
    return this.id as number;
  }

  public set setId(id: number) {
    this.id = id;
  }

  public get getFecha(): string {
    return this.fecha as string;
  }

  public set setFecha(fecha: string) {
    this.fecha = fecha;
  }

  public async listFecha(): Promise<IFecha | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<RowDataPacket[0]>(
        "SELECT * FROM fechas WHERE id = ?",
        [this.getId]
      );
      const success: IFecha = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async listFechas(): Promise<IFecha | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<RowDataPacket[0]>(
        "SELECT * FROM fechas"
      );
      const success: IFecha = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async insertFecha(): Promise<ISuccessOperation | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<ResultSetHeader>(
        "INSERT INTO fechas SET id = ?, fecha = ?",
        [this.getId, this.getFecha]
      );
      const success: ISuccessOperation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async updateFecha(): Promise<ISuccessOperation | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<ResultSetHeader>(
        "UPDATE fechas SET fecha = ? WHERE id = ?",
        [this.getFecha, this.getId]
      );
      const success: ISuccessOperation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async deleteFecha(): Promise<ISuccessOperation | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<ResultSetHeader>(
        "DELETE FROM fechas WHERE fecha = ?",
        [this.getFecha]
      );
      const success: ISuccessOperation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }
}

export default Fechas;