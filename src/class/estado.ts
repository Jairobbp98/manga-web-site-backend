import { IErrorDataBase, ISuccessOperation } from "../interfaces/database.interface";
import { IEstado, IEstadoClass } from "../interfaces/estado.interface";
import { RowDataPacket, ResultSetHeader } from 'mysql2/promise'
import Database from "./database";

class Estado implements IEstadoClass {
    public id?: number;
    public statuss?: string;
    private database: Database;

    constructor(estado?: IEstado) {
        this.id = estado?.id;
        this.statuss = estado?.statuss;
        this.database = new Database();
    }
    
    public get getId() : number {
        return this.id as number;
    }
    
    public set setId(id : number) {
        this.id = id;
    }
    
    public get getStatuss() : string {
        return this.statuss as string;
    }
    
    public set setStatuss(statuss : string) {
        this.statuss = statuss;
    }
    
    public async listEstado(): Promise<IEstado | IErrorDataBase>{
        const conn = await this.database.connection();
        try {
            const result = await conn.execute<RowDataPacket[0]>(
                'SELECT * FROM estado WHERE id = ?',
                [this.getId]
            );
            const success: IEstado = result[0];
            await conn.end();
            return success;
        } catch (err) {
            return err;
        }
    }
    
    public async listEstados(): Promise<IEstado | IErrorDataBase>{
        const conn = await this.database.connection();
        try {
            const result = await conn.execute<RowDataPacket[0]>(
                'SELECT * FROM estado'
            );
            const success: IEstado = result[0];
            await conn.end();
            return success;
        } catch (err) {
            return err;
        } 
    }
    
    public async insertEstado(): Promise<ISuccessOperation | IErrorDataBase>{
        const conn = await this.database.connection();
        try {
            const result = await conn.execute<ResultSetHeader>(
                'INSERT INTO estado SET id = ?, statuss = ?',
                [this.getId, this.getStatuss]
            );
            const success: ISuccessOperation = result[0];
            await conn.end();
            return success;
        } catch (err) {
            return err;
        }
    }
    
    public async updateEstado(): Promise<ISuccessOperation | IErrorDataBase>{
        const conn = await this.database.connection();
        try {
            const result = await conn.execute<ResultSetHeader>(
                'UPDATE estado SET statuss = ? WHERE id = ?',
                [this.getStatuss, this.getId]
            );
            const success: ISuccessOperation = result[0];
            await conn.end();
            return success;
        } catch (err) {
            return err;
        }
    }
    
    public async deleteEstado(): Promise<ISuccessOperation | IErrorDataBase>{
        const conn = await this.database.connection();
        try {
            const result = await conn.execute<ResultSetHeader>(
                'DELETE FROM estado WHERE id = ?',
                [this.getStatuss, this.getId]
            );
            const success: ISuccessOperation = result[0];
            await conn.end();
            return success;
        } catch (err) {
            return err;
        }       
    }   
}

export default Estado;