import Database from './database'
import { Request, Response } from 'express'

class Books {
  public id: number;
  public tipo: number;
  public id_information: number;
  public id_capitulos: number;
  public id_clasification: number;
  public id_likes: number;
  public conn = new Database();

  constructor(
    id: number,
    tipo: number,
    id_information: number,
    id_capitulos: number,
    id_clasification: number,
    id_likes: number
  ) {
    this.id = id;
      this.tipo = tipo;
      this.id_information = id_information;
      this.id_capitulos = id_capitulos;
      this.id_clasification = id_clasification;
      this.id_likes = id_likes;
  }

  insertBook(req: Request, res: Response) {
    
  }

  updateBook(req: Request, res: Response) {

  }

  deleteBook(req: Request, res: Response) {

  }

  listBook(req: Request, res: Response) {
    
  }
}
