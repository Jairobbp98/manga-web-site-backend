import express, { Application } from 'express';
import morgan from 'morgan';
import documentsRoutes from '../routes/documents.routes';

class Server {
    private app: Application;

    constructor(private port?: number | string) {
        this.app = express();
        this.config();
        this.middlewares();
        this.routes();
    }

    config(){
        this.app.set('port', process.env.PORT || this.port || 3000);
    }

    middlewares(){
        this.app.use(morgan('dev'));
        this.app.use(express.json());
    }

    routes(){
        this.app.use(documentsRoutes);
    }

    async listen(){
        await this.app.listen(this.app.get('port'));
        console.log('Server on port', this.app.get('port'));
    }
}

export { Server }