import { ResultSetHeader, RowDataPacket } from "mysql2";
import Database from "../class/database";
import { IDocuments, IDocumentsClass } from "../interfaces/documents.interface";
import {
  IErrorDataBase,
  ISuccessOperation,
} from "../interfaces/database.interface";
import path from "path";

class Documents implements IDocumentsClass {
  public id?: number;
  public direction?: string;
  private database: Database;

  constructor(doc?: IDocuments) {
    this.id = doc?.id;
    this.direction = path.join(__dirname, doc?.direction as string);
    this.database = new Database();
  }

  /* Setters && Getters */
  public get getId(): number {
    return this.id as number;
  }

  public set setId(id: number) {
    this.id = id;
  }

  public get getDirection(): string {
    return this.direction as string;
  }

  public set setDirection(direction: string) {
    this.direction = direction;
  }

  /* Methods */
  public async listDocuments(): Promise<IDocuments | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<RowDataPacket[0]>(
        "SELECT * FROM documents"
      );
      const list: IDocuments = result[0];
      await conn.end();
      return list;
    } catch (err) {
      return err;
    }
  }

  public async listDocument(): Promise<IDocuments | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<RowDataPacket[0]>(
        "SELECT * FROM documents WHERE id = ?",
        [this.getId]
      );
      const list: IDocuments = result[0];
      await conn.end();
      return list;
    } catch (err) {
      return err;
    }
  }

  public async insertDocument(): Promise<ISuccessOperation | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<ResultSetHeader>(
        "INSERT INTO documents SET id = ?, direction = ?",
        [this.getId, this.getDirection]
      );
      const success: ISuccessOperation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async updateDocument(): Promise<ISuccessOperation | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<ResultSetHeader>(
        "UPDATE documents SET direction = ? WHERE id = ?",
        [this.getDirection, this.getId]
      );
      const success: ISuccessOperation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async deleteDocument(): Promise<ISuccessOperation | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<ResultSetHeader>(
        "DELETE FROM documents WHERE direction = ?",
        [this.getDirection]
      );
      const success: ISuccessOperation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }
}

export default Documents;
