import { ResultSetHeader, RowDataPacket } from "mysql2/promise";
import {
  IErrorDataBase,
  ISuccessOperation,
} from "../interfaces/database.interface";
import { IGenders, IGendersClass } from "../interfaces/genders.interface";
import Database from "./database";

class Genders implements IGendersClass {
  public id?: number;
  public gender?: string;
  private database: Database;

  constructor(genders?: IGenders) {
    this.id = genders?.id;
    this.gender = genders?.gender;
    this.database = new Database();
  }

  /* Setters && Getters */
  public get getId(): number {
    return this.id as number;
  }

  public set setId(id: number) {
    this.id = id;
  }

  public get getGender(): string {
    return this.gender as string;
  }

  public set setGender(gender: string) {
    this.gender = gender;
  }

  /* Methods */
  public async listGender(): Promise<IGenders | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<RowDataPacket[0]>(
        "SELECT * FROM genders WHERE nom_gender = ?",
        [this.getGender]
      );
      const success: IGenders = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async listGenders(): Promise<IGenders | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<RowDataPacket[0]>(
        "SELECT * FROM genders"
      );
      const success: IGenders = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async insertGender(): Promise<ISuccessOperation | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<ResultSetHeader>(
        "INSERT INTO gender SET id = ?, nom_gender = ?",
        [this.getId, this.getGender]
      );
      const success: ISuccessOperation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async updateGender(): Promise<ISuccessOperation | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<ResultSetHeader>(
        "UPDATE gender SET nom_gender = ? WHERE id = ?",
        [this.getGender, this.getId]
      );
      const success: ISuccessOperation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async deleteGender(): Promise<ISuccessOperation | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<ResultSetHeader>(
        "DELETE FROM gender WHERE nom_gender = ?",
        [this.getGender]
      );
      const success: ISuccessOperation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }   
  }
}

export default Genders;