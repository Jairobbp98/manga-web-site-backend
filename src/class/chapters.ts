import { IChapters, IChaptersClass } from "../interfaces/chapters.interface";
import Database from "./database";

class Chapters implements IChaptersClass {
    public id?: number;
    public nom_cap?: string;
    public id_img_port?: number;
    public id_fecha_estreno?: number;
    private database: Database;

    constructor(chapters?: IChapters) {
        this.id = chapters?.id;
        this.nom_cap = chapters?.nom_cap;
        this.id_img_port = chapters?.id_img_port;
        this.id_fecha_estreno = chapters?.id_fecha_estreno;
        this.database = new Database();
    }
}