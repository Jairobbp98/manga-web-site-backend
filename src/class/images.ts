import { ResultSetHeader, RowDataPacket } from 'mysql2/promise';
import { IErrorDataBase, ISuccessOperation } from "../interfaces/database.interface";
import { IImages, IImagesClass } from "../interfaces/images.interface";
import Database from "./database";

class Images implements IImagesClass {
    public id?: number;
    public dirImage?: string;
    private database: Database;

    constructor(images?: IImages) {
        this.id = images?.id;
        this.dirImage = images?.img;
        this.database = new Database();
    }

    /* Setters && Getters */
    public get getId() : number {
        return this.id as number;
    }

    public set setId(id: number){
        this.id = id;
    }

    public get getDirImages() : string {
        return this.dirImage as string;
    }
    
    public set setDirImages(dirImages : string) {
        this.dirImage = dirImages;
    }
    
    /* Methods */
    public async listImage(): Promise<IImages | IErrorDataBase> {
        const conn = await this.database.connection();
        try {
            const result = await conn.execute<RowDataPacket[0]>(
                'SELECT * FROM images WHERE img_url = ?',
                [this.getDirImages]
            );
            const success: IImages = result[0];
            await conn.end();
            return success;
        } catch (err) {
            return err;
        }        
    }
    
    public async listImages(): Promise<IImages | IErrorDataBase> {
        const conn = await this.database.connection();
        try {
            const result = await conn.execute<RowDataPacket[0]>(
                'SELECT * FROM images'
            );
            const success: IImages = result[0];
            await conn.end();
            return success;
        } catch (err) {
            return err;
        }
    }
    
    public async insertImage(): Promise<ISuccessOperation | IErrorDataBase> {
        const conn = await this.database.connection();
        try {
            const result = await conn.execute<ResultSetHeader>(
                'INSERT INTO images SET id = ?, nom_gender = ?',
                [this.getId, this.getDirImages]
            );
            const success: ISuccessOperation = result[0];
            await conn.end();
            return success;
        } catch (err) {
            return err;
        }
    }
    
    public async updateImage(): Promise<ISuccessOperation | IErrorDataBase> {
        const conn = await this.database.connection();
        try {
            const result = await conn.execute<ResultSetHeader>(
                'UPDATE images SET nom_gender = ? WHERE id = ?',
                [this.getDirImages, this.getId]
            );
            const success: ISuccessOperation = result[0];
            await conn.end();
            return success;
        } catch (err) {
            return err;
        }
    }
    
    public async deleteImage(): Promise<ISuccessOperation | IErrorDataBase> {
        const conn = await this.database.connection();
        try {
            const result = await conn.execute<ResultSetHeader>(
                'DELETE FROM images WHERE id = ?',
                [this.getDirImages, this.getId]
            );
            const success: ISuccessOperation = result[0];
            await conn.end();
            return success;
        } catch (err) {
            return err;
        }
    }
}

export default Images;