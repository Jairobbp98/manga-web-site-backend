import {
  IErrorDataBase,
  ISuccessOperation,
} from "../interfaces/database.interface";
import {
  IInformation,
  IInformationClass,
} from "../interfaces/information.interface";
import { RowDataPacket, ResultSetHeader } from "mysql2/promise";
import Database from "./database";

class Information implements IInformationClass {
  public id?: number;
  public idTitulo?: number;
  public idImgPort?: number;
  public idDoc?: number;
  public idAuthors?: number;
  public idGeneros?: number;
  public idFechaEstreno?: number;
  public idEstado?: number;
  public idDescripcion?: number;
  private database: Database;

  constructor(information?: IInformation) {
    this.id = information?.id;
    this.idTitulo = information?.idTitulo;
    this.idImgPort = information?.idImgPort;
    this.idDoc = information?.idDoc;
    this.idAuthors = information?.idAuthors;
    this.idGeneros = information?.idGeneros;
    this.idFechaEstreno = information?.idFechaEstreno;
    this.idEstado = information?.idEstado;
    this.idDescripcion = information?.idDescripcion;
    this.database = new Database();
  }

  /* Setters && Getters */
  public get getId(): number {
    return this.id as number;
  }
  public get getIdTitulo(): number {
    return this.idTitulo as number;
  }
  public get getIdImgPort(): number {
    return this.idImgPort as number;
  }
  public get getIdDoc(): number {
    return this.idDoc as number;
  }
  public get getIdAuthors(): number {
    return this.idAuthors as number;
  }
  public get getIdGeneros(): number {
    return this.idGeneros as number;
  }
  public get getIdFechaEstreno(): number {
    return this.idFechaEstreno as number;
  }
  public get getIdEstado(): number {
    return this.idEstado as number;
  }
  public get getIdDescripcion(): number {
    return this.idDescripcion as number;
  }

  public set setId(id: number) {
    this.id = id;
  }

  public set setIdTitulo(idTitulo: number) {
    this.idTitulo = idTitulo;
  }

  public set setIdImgPort(idImgPort: number) {
    this.idImgPort = idImgPort;
  }

  public set setIdDoc(idDoc: number) {
    this.idDoc = idDoc;
  }

  public set setIdAuthors(idAuthors: number) {
    this.idAuthors = idAuthors;
  }

  public set setIdGeneros(idGeneros: number) {
    this.idGeneros = idGeneros;
  }

  public set setIdFechaEstreno(idFechaEstreno: number) {
    this.idFechaEstreno = idFechaEstreno;
  }

  public set setIdEstado(idEstado: number) {
    this.idEstado = idEstado;
  }

  public set setIdDescripcion(idDescripcion: number) {
    this.idDescripcion = idDescripcion;
  }

  public async listInformations(): Promise<IInformation | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<RowDataPacket[0]>(
        "SELECT * FROM information"
      );
      const success: IInformation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async listInformation(): Promise<IInformation | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<RowDataPacket[0]>(
        "SELECT * FROM information WHERE id = ?",
        [this.getId]
      );
      const success: IInformation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async insertInformation(): Promise<
    ISuccessOperation | IErrorDataBase
  > {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<ResultSetHeader>(
        "INSERT INTO information SET id = ?, id_title = ?, id_img_port = ?, id_doc = ?, id_authors = ?, id_generos = ?, id_fecha_estreno = ?, id_status = ?, id_description = ?",
        [
          this.getId,
          this.getIdTitulo,
          this.getIdImgPort,
          this.getIdDoc,
          this.getIdAuthors,
          this.getIdGeneros,
          this.getIdFechaEstreno,
          this.getIdEstado,
          this.getIdDescripcion,
        ]
      );
      const success: ISuccessOperation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async updateInformation(): Promise<
    ISuccessOperation | IErrorDataBase
  > {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<ResultSetHeader>(
        "UPDATE information SET id_title = ?, id_img_port = ?, id_doc = ?, id_authors = ?, id_generos = ?, id_fecha_estreno = ?, id_status = ?, id_description = ? WHERE id = ?",
        [
          this.getIdTitulo,
          this.getIdImgPort,
          this.getIdDoc,
          this.getIdAuthors,
          this.getIdGeneros,
          this.getIdFechaEstreno,
          this.getIdEstado,
          this.getIdDescripcion,
          this.getId,
        ]
      );
      const success: ISuccessOperation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async deleteInformation(): Promise<
    ISuccessOperation | IErrorDataBase
  > {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<ResultSetHeader>(
        "DELETE FROM information WHERE id = ?",
        [this.getId]
      );
      const success: ISuccessOperation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }
}

export default Information;