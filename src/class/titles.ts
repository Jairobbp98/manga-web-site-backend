import { RowDataPacket, ResultSetHeader } from "mysql2/promise";
import {
  IErrorDataBase,
  ISuccessOperation,
} from "../interfaces/database.interface";
import { ITitles, ITitlesClass } from "../interfaces/titles.interface";
import Database from "./database";

class Titles implements ITitlesClass {
  public id?: number;
  public principalTitle?: string;
  public otherTitle?: string;
  private database: Database;

  constructor(title?: ITitles) {
    this.id = title?.id;
    this.principalTitle = title?.principalTitle;
    this.otherTitle = title?.otherTitle;
    this.database = new Database();
  }

  public get getId(): number {
    return this.id as number;
  }

  public set setId(id: number) {
    this.id = id;
  }

  public get getPrincipalTitle(): string {
    return this.principalTitle as string;
  }

  public set setPrincipalTitle(principalTitle: string) {
    this.principalTitle = principalTitle;
  }

  public get getOtherTitle(): string {
    return this.otherTitle as string;
  }

  public set setOtherTitle(otherTitle: string) {
    this.otherTitle = otherTitle;
  }

  public async listTitle(): Promise<ITitles | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<RowDataPacket[0]>(
        "SELECT * FROM titles WHERE id = ?",
        [this.getId]
      );
      const success: ITitles = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async listTitles(): Promise<ITitles | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<RowDataPacket[0]>(
        "SELECT * FROM titles"
      );
      const success: ITitles = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async insertTitle(): Promise<ISuccessOperation | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<ResultSetHeader>(
        "INSERT INTO titles SET id = ?, principal_title = ?, other_title = ?",
        [this.getId, this.getPrincipalTitle, this.getOtherTitle]
      );
      const success: ISuccessOperation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async updateTitle(): Promise<ISuccessOperation | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<ResultSetHeader>(
        "UPDATE titles SET principal_title = ?, other_title = ? WHERE id = ?",
        [this.getPrincipalTitle, this.getOtherTitle, this.getId]
      );
      const success: ISuccessOperation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async deleteTitle(): Promise<ISuccessOperation | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<ResultSetHeader>(
        "DELETE FROM titles WHERE id = ?",
        [this.getId]
      );
      const success: ISuccessOperation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }
}

export default Titles;