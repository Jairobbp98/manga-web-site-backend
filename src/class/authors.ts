import { ResultSetHeader, RowDataPacket } from "mysql2/promise";
import Database from "../class/database";
import { IAuthors, IAuthorsClass } from '../interfaces/authors.interface'
import { IErrorDataBase, ISuccessOperation } from "../interfaces/database.interface";

class Authors implements IAuthorsClass {
  public id?: number;
  public nameAuthor?: string;
  private database: Database;

  constructor(author?: IAuthors) {
    this.id = author?.id;
    this.nameAuthor = author?.nameAuthor;
    this.database = new Database();
  }

  /* Setters && Getters */
  public set setId(id: number){
      this.id = id;
  }

  public get getId(): number{
      return this.id as number;
  }

  public set setNameAuthor(nameAuthor: string){
      this.nameAuthor = nameAuthor;
  }

  public get getNameAuthor(): string{
      return this.nameAuthor as string;
  }

  /* Methods */
  public async listAuthor(): Promise<IAuthors | IErrorDataBase>{
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<RowDataPacket[0]>('SELECT * FROM authors WHERE nom_author = ?', [this.getNameAuthor]);
      const success: IAuthors = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async listAuthors(): Promise<IAuthors | IErrorDataBase>{
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<RowDataPacket[0]>('SELECT * FROM authors');
      const success: IAuthors = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async insertAuthor(): Promise<ISuccessOperation | IErrorDataBase>{
      const conn = await this.database.connection();
      try {
        const result = await conn.execute<ResultSetHeader>(
          'INSERT INTO authors SET id = ?, nom_author = ?',
          [this.getId, this.getNameAuthor]);
        const success: ISuccessOperation = result[0];
        await conn.end();
        return success;
      } catch (err) {
        return err
      }
  }

  public async updateAuthor(): Promise<ISuccessOperation | IErrorDataBase>{
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<ResultSetHeader>(
        'UPDATE authors SET nom_author = ? WHERE id = ?',
        [this.getNameAuthor, this.getId]
      );
      const success: ISuccessOperation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async deleteAuthor(): Promise<ISuccessOperation | IErrorDataBase>{
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<ResultSetHeader>(
        'DELETE FROM authors WHERE nom_author = ?',
        [this.getNameAuthor]
      );
      const success: ISuccessOperation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }
}

export default Authors;