import { RowDataPacket, ResultSetHeader } from "mysql2/promise";
import {
  IErrorDataBase,
  ISuccessOperation,
} from "../interfaces/database.interface";
import {
  IDescription,
  IDescriptionClass,
} from "../interfaces/description.interface";
import Database from "./database";

class Description implements IDescriptionClass {
  public id?: number;
  public descripcion?: string;
  private database: Database;

  constructor(description?: IDescription) {
    this.id = description?.id;
    this.descripcion = description?.descripcion;
    this.database = new Database();
  }

  public get getId(): number {
    return this.id as number;
  }

  public get getDescription(): string {
    return this.descripcion as string;
  }

  public set setId(id: number) {
    this.id = id;
  }

  public set setDescription(description: string) {
    this.descripcion = description;
  }

  public async listDescription(): Promise<IDescription | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<RowDataPacket[0]>(
        "SELECT * FROM descripcion WHERE id = ?",
        [this.getId]
      );
      const success: IDescription = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async listDescriptions(): Promise<IDescription | IErrorDataBase> {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<RowDataPacket[0]>(
        "SELECT * FROM descripcion"
      );
      const success: IDescription = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async insertDescription(): Promise<
    ISuccessOperation | IErrorDataBase
  > {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<ResultSetHeader>(
        "INSERT INTO descripcion SET id = ?, txtDescripcion = ?",
        [this.getId, this.getDescription]
      );
      const success: ISuccessOperation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async updateDescription(): Promise<
    ISuccessOperation | IErrorDataBase
  > {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<ResultSetHeader>(
        "UPDATE descripcion SET txtDescripcion = ? WHERE id = ?",
        [this.getDescription, this.getId]
      );
      const success: ISuccessOperation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }

  public async deleteDescription(): Promise<
    ISuccessOperation | IErrorDataBase
  > {
    const conn = await this.database.connection();
    try {
      const result = await conn.execute<ResultSetHeader>(
        "DELETE FROM descripcion WHERE id = ?",
        [this.getId]
      );
      const success: ISuccessOperation = result[0];
      await conn.end();
      return success;
    } catch (err) {
      return err;
    }
  }
}

export default Description;