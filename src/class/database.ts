import { createPool, Pool} from "mysql2/promise";
import { IConfigDatabase, IDatabase }  from "../interfaces/database.interface";

class Database implements IDatabase {
  private host: string;
  private user: string;
  private password: string;
  private database: string;
  private connectionLimit: number;

  constructor() {
    this.host = process.env.HOST || "localhost";
    this.user = "root";
    this.password = "87709889";
    this.database = "tmangaMysql";
    this.connectionLimit = 10;
  }

  private config(): IConfigDatabase {
    const conexionConfig: IConfigDatabase = {
      host: this.host,
      user: this.user,
      password: this.password,
      database: this.database,
      connectionLimit: this.connectionLimit
    }
    return conexionConfig;
  }

  public async connection(): Promise<Pool> {
    const conexion: Pool = createPool(this.config());
    return conexion;
  }
}

export default Database;