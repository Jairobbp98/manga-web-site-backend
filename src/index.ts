import { Server } from './class/server';

const main = (): void => {
    const app = new Server();
    app.listen();
}

main();